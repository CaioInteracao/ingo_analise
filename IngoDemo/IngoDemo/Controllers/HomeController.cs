﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IngoDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Exemplo do autofill, não funciona por motivos de teste";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Exemplo do Social e autenticação";

            return View();
        }
    }
}